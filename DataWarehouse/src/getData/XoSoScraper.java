package getData;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;

public class XoSoScraper {

    public static void main(String[] args) {
        try {
            // Kết nối tới trang web xskt.com.vn
            Document doc = Jsoup.connect("https://xskt.com.vn/").get();
//            System.out.println("Tỉnh/Thành phố: " + doc);
            // Trích xuất thông tin xổ số
            Elements xosoElements = doc.select(".box-ketqua");
            System.out.println(xosoElements);
            
            for (Element xosoElement : xosoElements) {
                String province = xosoElement.select("h2").text();
                String result = xosoElement.select(".box-table .result td").text();
                System.out.println("Tỉnh/Thành phố: " + province);
                System.out.println("Kết quả xổ số: " + result);
                System.out.println("-----------------------------");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

