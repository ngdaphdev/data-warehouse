package getData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WebDataFetcher {
	public static void main(String[] args) {
		String urlStr = "https://xskt.com.vn"; // Thay thế bằng URL của trang web bạn muốn lấy dữ liệu

		try {
			// Tạo URL từ địa chỉ web
			URL url = new URL(urlStr);

			// Mở kết nối HTTP
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			// Thiết lập phương thức yêu cầu là GET
			conn.setRequestMethod("GET");

			// Đọc dữ liệu từ kết nối
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			StringBuilder content = new StringBuilder();

			while ((line = reader.readLine()) != null) {
				content.append(line);
			}

			// Đóng kết nối
			conn.disconnect();

			// Hiển thị nội dung lấy được từ trang web
			System.out.println(content.toString());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
